Source: python-getdns
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ondřej Surý <ondrej@debian.org>,
           Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               libevent-dev,
               libgetdns-dev,
               libldns-dev,
               python3-all-dev,
               python3-setuptools,
               python3-sphinx,
Standards-Version: 4.7.0
Homepage: https://getdnsapi.org/
Vcs-Git: https://salsa.debian.org/python-team/packages/python-getdns.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-getdns
Rules-Requires-Root: no

Package: python3-getdns
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Suggests: python-getdns-doc
Multi-Arch: same
Description: modern asynchronous DNS API (python 3 bindings)
 getdns is a modern asynchronous DNS API.  It implements DNS entry
 points from a design developed and vetted by application developers,
 in an API specification edited by Paul Hoffman.  With the development
 of this API, we intend to offer application developers a modernized
 and flexible way to access DNS security (DNSSEC) and other powerful
 new DNS features; a particular hope is to inspire application
 developers towards innovative security solutions in their
 applications.
 .
 This package contains python 3 bindings for the library.

Package: python-getdns-doc
Section: doc
Architecture: all
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Suggests: python3-getdns
Multi-Arch: foreign
Description: modern asynchronous DNS API (documentation)
 getdns is a modern asynchronous DNS API.  It implements DNS entry
 points from a design developed and vetted by application developers,
 in an API specification edited by Paul Hoffman.  With the development
 of this API, we intend to offer application developers a modernized
 and flexible way to access DNS security (DNSSEC) and other powerful
 new DNS features; a particular hope is to inspire application
 developers towards innovative security solutions in their
 applications.
 .
 This package contains documentation for the python bindings.

python-getdns (1.0.0~b1-6) unstable; urgency=medium

  * Team upload.
  * Add d/source/options extend-diff-ignore to fix dpkg-source failure due to
    local changes (python package metadata regeneration) (Closes: #1047820)
  * Bump standards-version to 4.7.0 without further change
  * Remove myself from uploaders

 -- Scott Kitterman <scott@kitterman.com>  Sun, 11 Aug 2024 10:03:07 -0400

python-getdns (1.0.0~b1-5) unstable; urgency=medium

  [ Scott Kitterman ]
  * Remove obsolete python-getdns-doc Suggests: python-getdns

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 23 Nov 2022 00:26:20 +0000

python-getdns (1.0.0~b1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on libgetdns-dev and
      python3-sphinx.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Oct 2022 02:29:42 +0100

python-getdns (1.0.0~b1-3) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Bump standards version to 4.5.0.
  * Set Rules-Requires-Root: no

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python3-getdns: Add Multi-Arch: same.
    + python-getdns-doc: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Scott Kitterman ]
  * Fix debian/watch

 -- Scott Kitterman <scott@kitterman.com>  Wed, 15 Dec 2021 21:16:37 -0500

python-getdns (1.0.0~b1-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Use Python 3 for building docs.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Sat, 27 Jul 2019 01:37:19 +0200

python-getdns (1.0.0~b1-1) unstable; urgency=medium

  * new upstream version

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 16 Jul 2016 11:19:56 +0200

python-getdns (0.6.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Scott Kitterman ]
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Thu, 07 Apr 2016 01:11:29 -0400

python-getdns (0.6.0-1) unstable; urgency=medium

  * Imported Upstream version 0.6.0
  * Update patches for 0.6.0 release

 -- Ondřej Surý <ondrej@debian.org>  Wed, 27 Jan 2016 10:44:54 +0100

python-getdns (0.5.0-2) unstable; urgency=medium

  * Backport upstream fix to back out STARTTLS support to align to
    upstream getdns changes (Closes: #810260)

 -- Scott Kitterman <scott@kitterman.com>  Fri, 15 Jan 2016 15:22:12 -0500

python-getdns (0.5.0-1) unstable; urgency=medium

  [ SVN-Git Migration ]
  * Migrate packaging to git with git-dpm

  [ Scott Kitterman ]
  * New upstream release
  * Agreed maintainer change to DPMT
    - Update maintainer/uploaders and point to DPMT git repository in debian/
      control
  * Add support for python3
    - Create new python3-getdns binary
    - Split documentation and examples into separate python-getdns-doc package
      so they are not duplicated
    - Adjust debian/rules and install files
  * Update copyright year in debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Tue, 29 Dec 2015 12:52:27 -0500

python-getdns (0.3.1-2) unstable; urgency=medium

  * import upstream fix for segfault from Context.run()
  * fix package description

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 17 Jun 2015 14:38:10 -0400

python-getdns (0.3.1-1) unstable; urgency=medium

  * New Upstream version (Closes: #783786)
  * joined the packaging team, adding myself to uploaders.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 30 Apr 2015 00:55:28 -0400

python-getdns (0.2.1-2) unstable; urgency=medium

  * Merge extra Scott Kitterman's changes from 0.2.0-2 into the repository
   + Adjust build-depends to fix FTBFS:
    - Change python-all (>= 2.6.6-3) build-dep to python-all-dev (>= 2.6.6-3)
   + Add Build-depend on dh-python to ensure the latest version is used
  * Add Vcs-* to debian/control
  * No DPMT maintenance

 -- Ondřej Surý <ondrej@debian.org>  Thu, 26 Feb 2015 09:35:43 +0100

python-getdns (0.2.1-1) unstable; urgency=medium

  * New upstream version 0.2.1
  * Depend on getdns >= 0.1.6
  * Update patches for 0.2.1 release

 -- Ondřej Surý <ondrej@debian.org>  Wed, 25 Feb 2015 11:04:29 +0100

python-getdns (0.2.0-1) unstable; urgency=low

  * Initial release (Closes: #767890)
  * Build-Depend on fixed version of libgetdns-dev

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Nov 2014 11:21:39 +0100
